import sys
import re
import random

MIN_BET = 25

def y_n_query(question):
        while True:
            print('Please answer Yy/Nn:')
            response = input(question)
            if response == 'Y' or response == 'y':
                return True
            if response == 'N' or response == 'n':
                return False

class CurrentRoll:
    def __init__(self, line_bet, bank, real_dice=False, odds=0):
        self.real_dice = real_dice
        self.come_out = True
        self.line_bet = line_bet
        self.line = line_bet
        self.point = 0
        self.bank = bank
        self.place_payouts = {
            4 : (lambda x : 2.0 * x - 1),
            5 : (lambda x : 7.0 * x / 5),
            6 : (lambda x : 7.0 * x / 6),
            8 : (lambda x : 7.0 * x / 6),
            9 : (lambda x : 7.0 * x / 5),
            10 : (lambda x : 2.0 * x - 1)
            }

        self.odds_payouts = {
            4 : (lambda x : 2.0 * x),
            5 : (lambda x : 3.0 * x / 2),
            6 : (lambda x : 6.0 * x / 5),
            8 : (lambda x : 6.0 * x / 5),
            9 : (lambda x : 3.0 * x / 2),
            10 : (lambda x : 2.0 * x)
            }

        self.horn_payouts = {
            2 : (lambda x : 30.0 * x),
            3 : (lambda x : 15.0 * x),
            11 : (lambda x : 15.0 * x),
            12 : (lambda x : 30.0 * x),
            13 : (lambda x : 7.0 * x), # anycraps
            7 : (lambda x : 4.0 * x)
            }


        self.hardways_payouts = {
            4 : (lambda x : 7.0 * x),
            6 : (lambda x : 9.0 * x),
            8 : (lambda x : 9.0 * x),
            10 : (lambda x : 7.0 * x),
        }

        self.places = {4 : 0,
                       5 : 0,
                       6 : 0,
                       8 : 0,
                       9 : 0,
                       10 : 0}

        self.horn_bets = {
            2 : 0,
            3 : 0,
            11 : 0,
            12 : 0,
            13 : 0, #any craps
            7 : 0
            }

        self.hardways = {
            4 : 0,
            6 : 0,
            8 : 0,
            10 : 0
        }

    def place_line(self):
        self.bank = self.bank - self.line_bet

    def show_bank(self):
        print('You currently have $%.2f in bank' % self.bank)

    def walk(self):
        print('Thanks for playing!')
        self.show_bank()

    def generic_bet(self, bet_amount, min_bet, bet_type, bet_input_text, bet_placed_text):
        if bet_amount < min_bet:
            print('Not Valid Bet Amount, minimum bet is %d' % min_bet)
        bet_placed = False
        player_selection = input(bet_input_text)
        if player_selection.isdigit():
            player_selection = int(player_selection)
        if player_selection in bet_type.keys():
            bet_type[player_selection] += bet_amount
            self.bank = self.bank - bet_amount
            print(bet_placed_text % (bet_amount, player_selection))
            bet_placed = True
        return bet_placed



    def make_bet(self):
        while True:
            bet_amount = input('Input bet amount: ')
            if not bet_amount.isdigit():
                print('Not Valid Amount, try again.')
                continue
            bet_amount = int(bet_amount)
            if bet_amount > self.bank:
                print('Not Valid Amount, try again.')
                continue

            type_of_bet = input('Select from options:\n1) Place 2) Buy 3) Horn 4) Hardways 5) Cancel\n')
            if type_of_bet.isdigit():
                type_of_bet = int(type_of_bet)
            match type_of_bet:

                case 1:
                    bet_placed = self.generic_bet(bet_amount, MIN_BET, self.places,
                                                  'Select Number to Place 4, 5, 6, 8, 8, or 10. Any other to Cancel.\n ',
                                                  'Placed $%i %i')
                    if bet_placed:
                        break
                    else:
                        print('Cancelled place.\n')


                case 3:
                    bet_placed = self.generic_bet(bet_amount, MIN_BET/5, self.horn_bets,
                                                  'Select from horn 2, 3, 11, 12, Any 7 (7), or Any Craps (13). Any other to Cancel.\n',
                                                  'Placed $%i horn %i')
                    if bet_placed:
                        break
                    else:
                        print('Cancelled horn bet.\n')
                case 4:
                    bet_placed = self.generic_bet(bet_amount, MIN_BET/5, self.hardways,
                                                  'Select from Hardways 4, 6, 8, or 10. Any other to Cancel.\n',
                                                  'Placed $%i hard %i')
                    if bet_placed:
                        break
                    else:
                        print('Cancelled hardways bet.\n')

                case 5:
                    print('No bet\n')
                    break

    def clear_horns_and_hardways(self, die1, die2, dice_sum):
        if dice_sum not in [2, 3, 12] and self.horn_bets[13] != 0:
            print('Craps down.\n')
            self.horn_bets[13] = 0
        for x in self.horn_bets:
            if dice_sum != x and self.horn_bets[x] != 0:
                print('Horn %i down.\n' % dice_sum)
                self.horn_bets[x] = 0
        if dice_sum % 2 == 0 and die1 != die2 and self.hardways[dice_sum] != 0:
            print('Hard %s down.\n' % dice_sum)
            self.hardways[dice_sum] = 0


    def announce_dice(self, dice_sum, die1, die2):
        match dice_sum:
            case 2:
                print('2 craps. Aces!\n')
            case 3:
                print('3 craps. Ace Deuce!\n')
            case 4:
                announce_str = '4'
                if die1 == die2:
                    announce_str = announce_str + ', hard 4!'
                else:
                    announce_str = announce_str + '!'
                print(announce_str)
            case 5:
                print('5, Fever 5!')
            case 6:
                announce_str = '6'
                if die1 == die2:
                    announce_str = announce_str + ', hard 6!'
                else:
                    announce_str = announce_str + '!'
                print(announce_str)
            case 8:
                announce_str = '8'
                if die1 == die2:
                    announce_str = announce_str + ', hard 8!'
                else:
                    announce_str = announce_str + '!'
                print(announce_str)
            case 9:
                print('9, Nina, 9!')
            case 10:
                announce_str = '10'
                if die1 == die2:
                    announce_str = announce_str + ', hard 10!'
                else:
                    announce_str = announce_str + '!'
                print(announce_str)
            case 11:
                print('Yo, 11!')
            case 12:
                print('12!')

    def roll(self):

        if self.real_dice:
            while True:
                die1= input('Value of First Die (1-6):\n')
                if not die1.isdigit():
                    print('Not Valid Amount, try again.')
                    continue
                die1 = int(die1)
                if not die1 <= 6 and die1 > 0:
                    print('Not Valid Amount, try again.')
                    continue
                die2= input('Value of Second Die (1-6):\n')
                if not die2.isdigit():
                    print('Not Valid Amount, try again.')
                    continue
                die2 = int(die2)
                if not die2 <= 6 and die2 > 0:
                    print('Not Valid Amount, try again.')
                    continue
        else:
            die1 = random.randint(1,6)
            die2 = random.randint(1,6)

            
        dice_sum = die1 + die2

        self.announce_dice(dice_sum, die1, die2)

        self.clear_horns_and_hardways(die1, die2, dice_sum)
        if self.come_out:
            if dice_sum == 2 or \
               dice_sum == 3 or \
               dice_sum == 12:
                self.line = 0
                back_on_line = y_n_query('Craps, place new line bet?\n')
                if back_on_line:
                    self.line = self.place_line()
            if dice_sum == 7 or \
               dice_sum == 11:
                print('Frontline winner, pay the line.')
                self.bank = self.bank + self.line
                print('You won: ' + str(self.line), ' your total is: ' + str(self.bank))
            if dice_sum in [4, 5, 6, 8, 9, 10]:
                self.point = dice_sum
                print('Point is now %i' % dice_sum)
                self.come_out = False
        else:
            if dice_sum == self.point:
                print('Winner. Pay the line.\nYou won $%i.\n' % self.line)
                self.bank += self.line
                self.point = 0
                self.come_out = True
            if dice_sum == 7:
                print('Seven out.\n')
                self.point = 0
                self.come_out = True
            if dice_sum in self.places.keys() and self.places[dice_sum] != 0:
                bet = self.places[dice_sum]
                winnings =  self.place_payouts[dice_sum](bet)
                print('%i pays $%i.\n' % (dice_sum, winnings))
                self.bank += winnings






        

    def come_out_roll(self, die1, die2):
        return

    '''
    def roll(self, die1, die2):
        if (self.come_out)
    '''

def validate_user_integer(valid_inputs, user_input):
    return (user_input in valid_inputs)

    


def main():
    cr = CurrentRoll(25, 300)
    cr.show_bank()
    cr.place_line()

    while True:
        user_choice = input('Select from options: \n1) Roll 2) Bet 3) Check Balance 4) Walk\n')

        if user_choice.isdigit():
            user_choice = int(user_choice)

        match user_choice:
            case 1:
                cr.roll()
            case 2:
                cr.make_bet()
            case 3:
                cr.show_bank()
            case 4:
                cr.walk()
                break
            case _:
                print('\nInvalid option selected. Select again.\n')



main()
